package com.example.batuhan.route;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by batuhan on 5/1/15.
 */
//Get PlaceDetail Info from Google Places
public class PlaceDetails extends AsyncTask<String, Integer, String>{
    private GoogleMap mMap;
    private LatLng currentLoc;
    private Context context;

    String data = null;

    public PlaceDetails(GoogleMap mMap, LatLng currentLoc, Context context) {
        this.mMap = mMap;
        this.currentLoc = currentLoc;
        this.context = context;
    }

    // Invoked by execute() method of this object
    @Override
    protected String doInBackground(String... url) {
        try{
            data = downloadUrl(url[0]);
        }catch(Exception e){
            Log.d("Background Task",e.toString());
        }

        return data;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);


            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

    // Executed after the complete execution of doInBackground() method
    @Override
    protected void onPostExecute(String result){
        ParserTask parserTask = new ParserTask();

        // Start parsing the Google place details in JSON format
        // Invokes the "doInBackground()" method of the class ParseTask
        parserTask.execute(result);
    }

    private class ParserTask extends AsyncTask<String, Integer, HashMap<String,String>>{

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected HashMap<String,String> doInBackground(String... jsonData) {

            HashMap<String, String> hPlaceDetails = null;
            PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Start parsing Google place details in JSON format
                hPlaceDetails = placeDetailsJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return hPlaceDetails;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(HashMap<String,String> hPlaceDetails){

            String name = hPlaceDetails.get("name");
            String icon = hPlaceDetails.get("icon");
            String vicinity = hPlaceDetails.get("vicinity");
            String lat = hPlaceDetails.get("lat");
            String lng = hPlaceDetails.get("lng");
            String formatted_address = hPlaceDetails.get("formatted_address");
            String formatted_phone = hPlaceDetails.get("formatted_phone");
            String website = hPlaceDetails.get("website");
            String rating = hPlaceDetails.get("rating");
            String international_phone_number = hPlaceDetails.get("international_phone_number");
            String url = hPlaceDetails.get("url");

            double originlat = currentLoc.latitude;
            double originlog = currentLoc.longitude;

            LatLng dest = new LatLng(Double.valueOf(lat),Double.valueOf(lng));

            if (mMap != null) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(currentLoc).title("My Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
                mMap.addMarker(new MarkerOptions().position(dest).title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
            }

            LatLng midPoint = MapsActivity.midPoint(Double.valueOf(lat), Double.valueOf(lng), originlat, originlog);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(midPoint, 6), 3000, null);

            String direc = "http://maps.googleapis.com/maps/api/directions/json?origin=" + originlat + "," + originlog + "&destination=" + lat + "," + lng;

            if(new MapsActivity().getWaypoint() != null) {
                direc = "http://maps.googleapis.com/maps/api/directions/json?origin=" + originlat + "," + originlog + "&destination=" + lat + "," + lng + "&waypoints="
                        + new MapsActivity().getWaypoint().latitude + "," + new MapsActivity().getWaypoint().longitude;
            }

            Directions downloadTask = new Directions(mMap, currentLoc, context);
            downloadTask.execute(direc);
        }
    }

    private class PlaceDetailsJSONParser {
        /** Receives a JSONObject and returns a list */
        public HashMap<String,String> parse(JSONObject jObject){

            JSONObject jPlaceDetails = null;
            try {
                /** Retrieves all the elements in the 'places' array */
                jPlaceDetails = jObject.getJSONObject("result");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            /** Invoking getPlaces with the array of json object
             * where each json object represent a place
             */
            return getPlaceDetails(jPlaceDetails);
        }


        /** Parsing the Place Details Object object */
        private HashMap<String, String> getPlaceDetails(JSONObject jPlaceDetails){


            HashMap<String, String> hPlaceDetails = new HashMap<String, String>();

            String name = "-NA-";
            String icon = "-NA-";
            String vicinity="-NA-";
            String latitude="";
            String longitude="";
            String formatted_address="-NA-";
            String formatted_phone="-NA-";
            String website="-NA-";
            String rating="-NA-";
            String international_phone_number="-NA-";
            String url="-NA-";

            try {
                // Extracting Place name, if available
                if(!jPlaceDetails.isNull("name")){
                    name = jPlaceDetails.getString("name");
                }

                // Extracting Icon, if available
                if(!jPlaceDetails.isNull("icon")){
                    icon = jPlaceDetails.getString("icon");
                }

                // Extracting Place Vicinity, if available
                if(!jPlaceDetails.isNull("vicinity")){
                    vicinity = jPlaceDetails.getString("vicinity");
                }

                // Extracting Place formatted_address, if available
                if(!jPlaceDetails.isNull("formatted_address")){
                    formatted_address = jPlaceDetails.getString("formatted_address");
                }

                // Extracting Place formatted_phone, if available
                if(!jPlaceDetails.isNull("formatted_phone_number")){
                    formatted_phone = jPlaceDetails.getString("formatted_phone_number");
                }

                // Extracting website, if available
                if(!jPlaceDetails.isNull("website")){
                    website = jPlaceDetails.getString("website");
                }

                // Extracting rating, if available
                if(!jPlaceDetails.isNull("rating")){
                    rating = jPlaceDetails.getString("rating");
                }

                // Extracting rating, if available
                if(!jPlaceDetails.isNull("international_phone_number")){
                    international_phone_number = jPlaceDetails.getString("international_phone_number");
                }

                // Extracting url, if available
                if(!jPlaceDetails.isNull("url")){
                    url = jPlaceDetails.getString("url");
                }

                latitude = jPlaceDetails.getJSONObject("geometry").getJSONObject("location").getString("lat");
                longitude = jPlaceDetails.getJSONObject("geometry").getJSONObject("location").getString("lng");


                hPlaceDetails.put("name", name);
                hPlaceDetails.put("icon", icon);
                hPlaceDetails.put("vicinity", vicinity);
                hPlaceDetails.put("lat", latitude);
                hPlaceDetails.put("lng", longitude);
                hPlaceDetails.put("formatted_address", formatted_address);
                hPlaceDetails.put("formatted_phone", formatted_phone);
                hPlaceDetails.put("website", website);
                hPlaceDetails.put("rating", rating);
                hPlaceDetails.put("international_phone_number", international_phone_number);
                hPlaceDetails.put("url", url);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return hPlaceDetails;
        }
    }
}