package com.example.batuhan.route;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by batuhan on 5/1/15.
 */
public class Directions extends AsyncTask<String, Void, String> {
    private GoogleMap mMap;
    private LatLng currentLoc;
    private Context context;

    public Directions(GoogleMap mMap, LatLng currentLoc, Context context) {
        this.mMap = mMap;
        this.currentLoc = currentLoc;
        this.context = context;
    }

    @Override
    protected String doInBackground(String... url) {
        String data = "";
        try {
            HttpConnection http = new HttpConnection();
            data = http.readUrl(url[0]);
        } catch (Exception e) {
            Log.d("Background Task", e.toString());
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        new ParserTask().execute(result);
    }

    //Draw route and put markers on route
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;
            String poi;
            int radius, distance;

            float[] results = new float[1];
            double dist = 0.0;

            radius = new MapsActivity().getPOIRadius();
            distance = new MapsActivity().getPOIDistance();

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    String latlng = point.get("lat_lng");
                    String[] test= latlng.split("_");

                    double lat = Double.parseDouble(test[0]);
                    double lng = Double.parseDouble(test[1]);
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(4);
                polyLineOptions.color(Color.BLUE);
                List<LatLng> test = polyLineOptions.getPoints();

                for (int a = 0; a < test.size(); a++) {
                    if(a+1 < test.size()) {
                        Location.distanceBetween(test.get(a).latitude, test.get(a).longitude, test.get(a + 1).latitude, test.get(a + 1).longitude, results);
                        dist += results[0];

                        if (dist > distance) {
                            if (dist%distance < 50) {
                                //mMap.addMarker(new MarkerOptions().position(new LatLng(test.get(a).latitude, test.get(a).longitude)).title("test"));

                                poi = new MapsActivity().getPOITypeByName(context);
                                String placesSearchStr = null;//+getString(R.string.google_maps_key);//ADD KEY
                                try {
                                    placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                                            "json?location="+test.get(a).latitude+","+test.get(a).longitude+
                                            "&radius="+ String.valueOf(radius) +"&sensor=true" +
                                            "&types="+ URLEncoder.encode(poi, "UTF-8") +
                                            "&key=" + new MapsActivity().getKey();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                                //execute query
                                new Places(mMap).execute(placesSearchStr);
                            } else if (dist%distance < 100) {
                                //mMap.addMarker(new MarkerOptions().position(new LatLng(test.get(a).latitude, test.get(a).longitude)).title("test"));

                                poi = new MapsActivity().getPOITypeByName(context);
                                String placesSearchStr = null;//+getString(R.string.google_maps_key);//ADD KEY
                                try {
                                    placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                                            "json?location="+test.get(a).latitude+","+test.get(a).longitude+
                                            "&radius="+ String.valueOf(radius) +"&sensor=true" +
                                            "&types="+ URLEncoder.encode(poi, "UTF-8") +
                                            "&key=" + new MapsActivity().getKey();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                                //execute query
                                new Places(mMap).execute(placesSearchStr);
                            } else if (dist%distance < 150) {
                                //mMap.addMarker(new MarkerOptions().position(new LatLng(test.get(a).latitude, test.get(a).longitude)).title("test"));

                                poi = new MapsActivity().getPOITypeByName(context);
                                String placesSearchStr = null;//+getString(R.string.google_maps_key);//ADD KEY
                                try {
                                    placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                                            "json?location="+test.get(a).latitude+","+test.get(a).longitude+
                                            "&radius="+ String.valueOf(radius) +"&sensor=true" +
                                            "&types="+ URLEncoder.encode(poi, "UTF-8") +
                                            "&key=" + new MapsActivity().getKey();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                                //execute query
                                new Places(mMap).execute(placesSearchStr);
                            } else if (dist%distance < 200) {
                                //mMap.addMarker(new MarkerOptions().position(new LatLng(test.get(a).latitude, test.get(a).longitude)).title("test"));

                                poi = new MapsActivity().getPOITypeByName(context);
                                String placesSearchStr = null;//+getString(R.string.google_maps_key);//ADD KEY
                                try {
                                    placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                                            "json?location="+test.get(a).latitude+","+test.get(a).longitude+
                                            "&radius="+ String.valueOf(radius) +"&sensor=true" +
                                            "&types="+ URLEncoder.encode(poi, "UTF-8") +
                                            "&key=" + new MapsActivity().getKey();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                                //execute query
                                new Places(mMap).execute(placesSearchStr);
                            }
                        }
                    }
                }
                Log.d("dis test", String.valueOf(dist));
            }

            mMap.addPolyline(polyLineOptions);
        }
    }

    //HttpConnection to download Google Directions Request
    private class HttpConnection {
        public String readUrl(String mapsApiDirectionsUrl) throws IOException {
            String data = "";
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(mapsApiDirectionsUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                iStream = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        iStream));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                data = sb.toString();
                br.close();
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            } finally {
                iStream.close();
                urlConnection.disconnect();
            }
            return data;
        }
    }

    //JSON Parser for returned data from Google Directions Request
    private class PathJSONParser {

        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;
            try {
                jRoutes = jObject.getJSONArray("routes");
                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            Integer dist = 0;
                            dist = (Integer) ((JSONObject) ((JSONObject) jSteps
                                    .get(k)).get("distance")).get("value");
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps
                                    .get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                String lat_lng =  Double.toString(((LatLng) list.get(l)).latitude) + "_" + Double.toString(((LatLng) list.get(l)).longitude);
                                hm.put("lat_lng", lat_lng);
                                hm.put("dist", String.valueOf(dist));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return routes;
        }

        /**
         * Method Courtesy :
         * jeffreysambells.com/2010/05/27
         * /decoding-polylines-from-google-maps-direction-api-with-java
         * */
        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }
            return poly;
        }
    }
}