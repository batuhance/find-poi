package com.example.batuhan.route;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.HashMap;

public class MapsActivity extends FragmentActivity{

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    public static LatLng currentLoc, waypoint;

    private static final String TAG = "MapsActivity";

    public static String url;
    public static String placeid;
    public static String spinText;
    public static String API_KEY;

    public static int dist;
    public static int rad;

    private SlidingUpPanelLayout mLayout;

    public final Context a = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMap = fm.getMap();
        mMap.getUiSettings().setMapToolbarEnabled(false);

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        currentLoc = new LatLng(lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude(),lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude());

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLoc,10), 3000, null);
        mMap.addMarker(new MarkerOptions().position(currentLoc).title("My Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

        final Button route = (Button) findViewById(R.id.route);
        final Button arrow = (Button) findViewById(R.id.arrow);

        final Animation animRotateUp = AnimationUtils.loadAnimation(this, R.anim.anim_rotate_up);
        final Animation animRotateDown = AnimationUtils.loadAnimation(this, R.anim.anim_rotate_down);
        final Animation animFadeIn = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);
        final Animation animFadeOut = AnimationUtils.loadAnimation(this, R.anim.anim_fade_out);

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {

            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelExpanded(View panel) {
                Log.i(TAG, "onPanelExpanded");

            }

            @Override
            public void onPanelCollapsed(View panel) {
                Log.i(TAG, "onPanelCollapsed");

                route.startAnimation(animFadeOut);
                animFadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        route.setVisibility(View.GONE);
                        route.clearAnimation();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                arrow.startAnimation(animRotateDown);
            }

            @Override
            public void onPanelAnchored(View panel) {
                Log.i(TAG, "onPanelAnchored");

                route.setVisibility(View.VISIBLE);
                route.startAnimation(animFadeIn);

                arrow.startAnimation(animRotateUp);
            }

            @Override
            public void onPanelHidden(View panel) {
                Log.i(TAG, "onPanelHidden");
            }
        });

        mLayout.setAnchorPoint(0.25f);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);

        final AutoCompleteTextView autoCompView = (AutoCompleteTextView) findViewById(R.id.autoDest);
        autoCompView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                autoCompView.setText("");
                return false;
            }
        });
        autoCompView.setAdapter(new PlacesAutoComplete().new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
        autoCompView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //String str = (String) adapterView.getItemAtPosition(i);
                placeid = ((PlacesAutoComplete.GooglePlacesAutocompleteAdapter) adapterView.getAdapter()).resultList.get(i).toString().split("del")[1];
                url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeid + "&key=" + getString(R.string.google_maps_key);
            }
        });

        route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (url != null) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE))
                            .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

                    showDialog();
                } else {
                    String str = "Please enter a destination where you want to go!";
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                }
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                showMarkerDialog(marker);
            }
        });

        setKey(this.getString(R.string.google_maps_key));
    }

    public static LatLng midPoint(double lat1,double lon1,double lat2,double lon2){

        double dLon = Math.toRadians(lon2 - lon1);

        //convert to radians
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        lon1 = Math.toRadians(lon1);

        double Bx = Math.cos(lat2) * Math.cos(dLon);
        double By = Math.cos(lat2) * Math.sin(dLon);
        double lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
        double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);

        LatLng mid = new LatLng(Math.toDegrees(lat3),Math.toDegrees(lon3));

        //print out in degrees
        System.out.println(Math.toDegrees(lat3) + " " + Math.toDegrees(lon3));
        return mid;
    }

    public String getPOITypeByName (Context context) {
        int i = -1;

        for (String nm: context.getResources().getStringArray(R.array.poi_types)) {
            i++;
            if (nm.equals(spinText))
                break;
        }
        return context.getResources().getStringArray(R.array.poi_types_codes)[i];
    }

    public void showMarkerDialog(final Marker marker) {
        final Dialog dialog = new Dialog(a);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.marker_dialog_box);
        dialog.show();

        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView info = (TextView) dialog.findViewById(R.id.info);

        title.setText(marker.getTitle());
        info.setText(marker.getSnippet());

        setWaypoint(marker.getPosition());

        Button add = (Button) dialog.findViewById(R.id.addRoute);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlaceDetails details = new PlaceDetails(mMap, currentLoc, a);
                details.execute(url);
                dialog.hide();

                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
            }
        });
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(a);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_box);
        dialog.show();

        final Spinner spinner = (Spinner) dialog.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spinText = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final Button done = (Button) dialog.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlaceDetails details = new PlaceDetails(mMap, currentLoc, a);
                details.execute(url);
                dialog.hide();
            }
        });

        final TextView poiText = (TextView) dialog.findViewById(R.id.poi_text);
        poiText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                //System.out.println(MotionEvent.ACTION_UP);
                //Toast.makeText(a,"Select type of POI which you want to see on route!",Toast.LENGTH_SHORT).show();
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (poiText.getRight() - poiText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        Toast.makeText(a, "Select type of POI which you want to see on route!", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                return false;
            }
        });

        final TextView poiCenter = (TextView) dialog.findViewById(R.id.poi_center_text);
        poiCenter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                //System.out.println(MotionEvent.ACTION_UP);
                //Toast.makeText(a,"Select type of POI which you want to see on route!",Toast.LENGTH_SHORT).show();
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (poiCenter.getRight() - poiCenter.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        Toast.makeText(a, "Select distance between POI query centers on route!", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                return false;
            }
        });

        final TextView poiRadius = (TextView) dialog.findViewById(R.id.poi_radius_text);
        poiRadius.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                //System.out.println(MotionEvent.ACTION_UP);
                //Toast.makeText(a,"Select type of POI which you want to see on route!",Toast.LENGTH_SHORT).show();
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (poiRadius.getRight() - poiRadius.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        Toast.makeText(a, "Select radius for POIs how far from route!", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                return false;
            }
        });

        final TextView seekCenterText = (TextView) dialog.findViewById(R.id.seekCenterText);
        final SeekBar seekCenter = (SeekBar) dialog.findViewById(R.id.seekCenter);
        seekCenter.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                String s = String.valueOf((seekCenter.getProgress())) + " km";
                seekCenterText.setText(s);
                setPOIDistance(seekCenter.getProgress() * 1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        final TextView seekRadiusText = (TextView) dialog.findViewById(R.id.seekRadiusText);
        final SeekBar seekRadius = (SeekBar) dialog.findViewById(R.id.seekRadius);
        seekRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                String s = String.valueOf((seekRadius.getProgress()) * 100) + " m";
                seekRadiusText.setText(s);
                setPOIRadius(seekRadius.getProgress() * 100);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public int setPOIDistance (int distance) {
        dist = distance;
        return dist;
    }

    public int getPOIDistance () { return dist; }

    public int setPOIRadius (int radius) {
        rad = radius;
        return rad;
    }

    public int getPOIRadius () { return rad; }

    public LatLng setWaypoint (LatLng coor) {
        waypoint = coor;
        return waypoint;
    }

    public LatLng getWaypoint() { return waypoint; }

    public String setKey (String key) {
        API_KEY = key;
        return API_KEY;
    }

    public String getKey () { return API_KEY; }
}
