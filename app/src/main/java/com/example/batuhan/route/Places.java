package com.example.batuhan.route;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by batuhan on 5/1/15.
 */
//Get Place Info from Google Places
public class Places extends AsyncTask<String, Void, String> {

    private MarkerOptions[] places;
    private Marker[] placeMarkers;

    //instance variables for Marker icon drawable resources
    private int foodIcon, bankIcon, parkIcon, hospitalIcon, gasIcon, policeIcon, otherIcon;

    private GoogleMap mMap;

    public Places(GoogleMap mMap) {
        this.mMap = mMap;
    }


    @Override
    protected String doInBackground(String... placesURL) {
        //fetch places

        //build result as string
        StringBuilder placesBuilder = new StringBuilder();
        //process search parameter string(s)
        for (String placeSearchURL : placesURL) {
            HttpClient placesClient = new DefaultHttpClient();
            try {
                //try to fetch the data

                //HTTP Get receives URL string
                HttpGet placesGet = new HttpGet(placeSearchURL);
                //execute GET with Client - return response
                HttpResponse placesResponse = placesClient.execute(placesGet);
                //check response status
                StatusLine placeSearchStatus = placesResponse.getStatusLine();
                //only carry on if response is OK
                if (placeSearchStatus.getStatusCode() == 200) {
                    //get response entity
                    HttpEntity placesEntity = placesResponse.getEntity();
                    //get input stream setup
                    InputStream placesContent = placesEntity.getContent();
                    //create reader
                    InputStreamReader placesInput = new InputStreamReader(placesContent);
                    //use buffered reader to process
                    BufferedReader placesReader = new BufferedReader(placesInput);
                    //read a line at a time, append to string builder
                    String lineIn;

                    while ((lineIn = placesReader.readLine()) != null) {
                        placesBuilder.append(lineIn);
                    }
                }
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        return placesBuilder.toString();
    }
    //process data retrieved from doInBackground
    protected void onPostExecute(String result) {
        //parse place data returned from Google Places
        //remove existing markers
            if(placeMarkers!=null){
                for(int pm=0; pm<placeMarkers.length; pm++){
                    if(placeMarkers[pm]!=null)
                        placeMarkers[pm].remove();
                }
            }
        //get drawable IDs
        foodIcon = R.drawable.restaurant;
        bankIcon = R.drawable.atm;
        hospitalIcon = R.drawable.hospital;
        gasIcon = R.drawable.fillingstation;
        parkIcon = R.drawable.parking;
        policeIcon = R.drawable.police;
        otherIcon = R.drawable.other;

        placeMarkers = new Marker[20];
        try {
            //parse JSON

            //create JSONObject, pass stinrg returned from doInBackground
            JSONObject resultObject = new JSONObject(result);
            //get "results" array
            JSONArray placesArray = resultObject.getJSONArray("results");
            //marker options for each place returned
            places = new MarkerOptions[placesArray.length()];
            //loop through places
            for (int p=0; p<placesArray.length(); p++) {
                //parse each place
                //if any values are missing we won't show the marker
                boolean missingValue=false;
                LatLng placeLL=null;
                String placeName="";
                String vicinity="";
                int currIcon = otherIcon;
                try{
                    //attempt to retrieve place data values
                    missingValue=false;
                    //get place at this index
                    JSONObject placeObject = placesArray.getJSONObject(p);
                    //get location section
                    JSONObject loc = placeObject.getJSONObject("geometry")
                            .getJSONObject("location");
                    //read lat lng
                    placeLL = new LatLng(Double.valueOf(loc.getString("lat")),
                            Double.valueOf(loc.getString("lng")));
                    //get types
                    JSONArray types = placeObject.getJSONArray("types");
                    //loop through types
                    for(int t=0; t<types.length(); t++){
                        //what type is it
                        String thisType=types.get(t).toString();
                        //check for particular types - set icons
                        if(thisType.contains("food")){
                            currIcon = foodIcon;
                            break;
                        }
                        else if(thisType.contains("atm")){
                            currIcon = bankIcon;
                            break;
                        }
                        else if(thisType.contains("gas_station")){
                            currIcon = gasIcon;
                            break;
                        }
                        else if(thisType.contains("hospital")){
                            currIcon = hospitalIcon;
                            break;
                        }
                        else if(thisType.contains("parking")){
                            currIcon = parkIcon;
                            break;
                        }
                        else if(thisType.contains("police")){
                            currIcon = policeIcon;
                            break;
                        }
                    }
                    //vicinity
                    vicinity = placeObject.getString("vicinity");
                    //name
                    placeName = placeObject.getString("name");
                }
                catch(JSONException jse){
                    Log.v("PLACES", "missing value");
                    missingValue=true;
                    jse.printStackTrace();
                }
                //if values missing we don't display
                if(missingValue)	places[p]=null;
                else
                    places[p]=new MarkerOptions()
                            .position(placeLL)
                            .title(placeName)
                            .icon(BitmapDescriptorFactory.fromResource(currIcon))
                            .snippet(vicinity);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if(places!=null && placeMarkers!=null){
            for(int p=0; p<places.length && p<placeMarkers.length; p++){
                //will be null if a value was missing
                if(places[p]!=null)
                    placeMarkers[p]=mMap.addMarker(places[p]);
            }
        }
    }
}