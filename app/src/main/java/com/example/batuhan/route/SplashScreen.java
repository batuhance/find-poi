package com.example.batuhan.route;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class SplashScreen extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final Animation anim = AnimationUtils.loadAnimation(this, R.anim.anim_splash);
        final Animation animEnd = AnimationUtils.loadAnimation(this, R.anim.anim_splash_orig);
        final Animation fade_in = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);

        final TextView text = (TextView) findViewById(R.id.splashText);
        final ImageView map = (ImageView) findViewById(R.id.map);
        final ImageView img = (ImageView) findViewById(R.id.splash);
        img.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                img.startAnimation(animEnd);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animEnd.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                text.setVisibility(View.VISIBLE);
                text.startAnimation(fade_in);

                map.setVisibility(View.VISIBLE);
                map.startAnimation(fade_in);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fade_in.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(SplashScreen.this, MapsActivity.class);
                startActivity(intent);
                SplashScreen.this.finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
}
