## **POI ON THE ROUTE** ##

# **Introduction** #
The number of vehicles are increasing in traffic day by day. As a result, traffic jam increases and many problems occur. People travel longer time where they want to go. They lose a lot of time.

While driving, people may have many needs like fuel, refreshments, medicine, etc. They may not know the district they are driving in well, so, they may not want to change their direction. In this project, we will propose a solution to find alternative points of interest on a given route.

# **Technical Approach** #
* Split route in specific length chosen by user.
* Take the chosen length as center point of request.
* Take radius and poi* type from user to send request.
* Parse returned result and add specific markers on our route.

*POI : Point of Interest

# **Used Technologies** #
**Google Maps API**

* Used for showing map on mobile.

**Google Directions API**

* Used for drawing route between two points.

**Google Places API**

* Used for retrieving information about places. We use Place Details
WS* and Autocomplete WS as part of Places API.

*WS : Web Service

# **Conclusion** #
**What we have learned?**

We have learned Android programming, JSON Parsing and technologies explained above.

**What is the contribution of the Project?**

There isn't any app like this on Google Play Store. We achieve to reduce consumed time for
searching a poi while driving and not to deviate from our route.

**What will we do in the future for the Project?**

This app is a beta version because limitations of Google API quota. We are planning to put
it on Google Play Store for commercial usage. Also, we are planning to develop iOS and
Windows Phone versions.

# **Contributors** #

İsmail Batuhan NARCI - batuhannarci@gmail.com

Bilal Nuri KILINÇ - bilalnuri32@gmail.com